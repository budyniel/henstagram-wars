﻿using CommonTools.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HenstagramWars.Competitors.Schemas
{
    public class EnumerationSchemaParsingStrategy : ISchemaParsingStrategy
    {
        public double CalculateFitness(string textToParse)
        {
            // If name can't be extracted, no reason to calculate fitness.
            if (ExtractName(textToParse) == string.Empty)
                return 0;

            (string body, string henhouse, string hen) = ExtractOthers(textToParse);

            double categoryCount = 4; // name, body, henhouse, hen
            double currentScore = 1; // there exists a name at this point

            if (string.IsNullOrEmpty(body) == false)
                currentScore++;
            if (string.IsNullOrEmpty(henhouse) == false)
                currentScore++;
            if (string.IsNullOrEmpty(hen) == false)
                currentScore++;

            return currentScore / categoryCount;
        }

        public Competitor Parse(string textToParse)
        {
            string name = ExtractName(textToParse);
            (string body, string henhouse, string hen) = ExtractOthers(textToParse);

            string[] bodyAspects = ToAspects(body);
            string[] henhouseAspects = ToAspects(henhouse);
            string[] henAspects = ToAspects(hen);

            return new Competitor(name,
                henQuality: henAspects.Length,
                henHouse: henhouseAspects.Length,
                charisma: bodyAspects.Length);
        }

        private (string body, string henhouse, string hen) ExtractOthers(string textToParse)
        {
            string body = TextParserEngine.SafelyExtractSingleCapturedElement(
                pattern: @"I am a:(.+?)$", text: textToParse);

            string henhouse = TextParserEngine.SafelyExtractSingleCapturedElement(
                pattern: @"My henhouse is:(.+?)$", text: textToParse);

            string hen = TextParserEngine.SafelyExtractSingleCapturedElement(
                pattern: @"My prized hen is:(.+?)$", text: textToParse);

            return (body, henhouse, hen);
        }

        private static string ExtractName(string textToParse)
        {
            return TextParserEngine.SafelyExtractSingleCapturedElement(
                            pattern: @"My name is:(.+?)$", text: textToParse);
        }

        private string[] ToAspects(string commaDelimited)
        {
            var records = commaDelimited.Split(",").Select(r => r.Trim()).ToArray();
            
            // Not the best validation in existence, but good enough for a demonstration.
            if(records.Length == 0 || 
                ((records.Length == 1 ) && (records[0] == string.Empty)))
            {
                return new string[0];
            }
            else
            {
                return records;
            }
        }
    }
}
