﻿using HenstagramWars.Competitors.Schemas;
using HenstagramWars.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HenstagramWars.Competitors
{
    /// <summary>
    /// This factory shouldn't be static, because it has a state: registered parsing strategies.
    /// </summary>
    public class CompetitorFactory
    {
        IEnumerable<ISchemaParsingStrategy> _strategies;

        public CompetitorFactory()
        {
            // NOTE: adding new strategies here, to the list, you expand schemas.
            _strategies = new ISchemaParsingStrategy[] { 
                new AspectSchemaParsingStrategy(),
                new SequentialSchemaParsingStrategy(),
                new EnumerationSchemaParsingStrategy()};
        }

        public List<Competitor> BuildCompetitors()
        {
            // WARNING: for this to work, files in _Data folder need to have the Property
            // 'Copy to output directory' set to 'Copy Always' or 'Copy if Newer'.
            // Otherwise, change the relative data path to make it work.
            string[] competitorTexts = ExtractCompetitorTexts("./_Data").ToArray();

            // Time to try to build as many competitors out of potentially damaged and malformed
            // input data as possible.
            var competitors = new List<Competitor>();
            foreach(string ct in competitorTexts)
            {
                // First, check which strategy is the most likely one.
                (double, ISchemaParsingStrategy) bestAdaptation = (0, new NullObjectParsingStrategy());
                foreach(var strategy in _strategies)
                {
                    double fitness = strategy.CalculateFitness(ct);
                    if (fitness > bestAdaptation.Item1)
                        bestAdaptation = (fitness, strategy);
                }

                // Having the best adaptation, time to parse it to create most probable Competitor
                Competitor competitor = bestAdaptation.Item2.Parse(ct);

                // If it was a NullObject, do not add it to the list of competitors.
                if (competitor != null)
                    competitors.Add(competitor);
            }

            return competitors;
        }

        private IEnumerable<string> ExtractCompetitorTexts(string dataPath)
        {
            FileRecord[] frs = FileOps.ReadFiles(dataPath);
            IEnumerable<string> competitorTexts = frs.Select(fr => fr.Content);
            return competitorTexts;
        }
    }
}
