﻿using HenstagramWars.Competitors;
using System;
using System.Collections.Generic;
using System.Text;

namespace HenstagramWars.Contests.Comparators
{
    public class AlwaysLeftComparisonStrategy : IComparisonStrategy
    {
        public Competitor CalculateWinner(Competitor competitor1, Competitor competitor2)
        {
            return competitor1;
        }
    }
}
