﻿using HenstagramWars.Competitors;
using HenstagramWars.Competitors.Schemas;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace HenstagramWarsTest.CompetitorConstruction
{
    class TestSequentialSchemaParsers
    {
        [Test]
        public void ProperlyParseValidRecord()
        {
            //Given
            string natalie = @"Natalie Newplan, 7, 4, 6";

            //When
            Competitor actual = new SequentialSchemaParsingStrategy().Parse(natalie);

            //Then
            Assert.AreEqual("Natalie Newplan", actual.Name);
            Assert.IsTrue(actual.HenQuality == 7);
            Assert.IsTrue(actual.HenHouse == 4);
            Assert.IsTrue(actual.Charisma == 6);
        }

        [Test]
        public void ProperlyParseMinimumValidRecord()
        {
            //Given
            string natalie = @"Natalie Newplan";

            //When
            Competitor actual = new SequentialSchemaParsingStrategy().Parse(natalie);

            //Then
            Assert.AreEqual(actual.Name, "Natalie Newplan");
            Assert.IsTrue(actual.HenQuality == 0);
            Assert.IsTrue(actual.HenHouse == 0);
            Assert.IsTrue(actual.Charisma == 0);
        }

        [Test]
        public void CalculateFitnessForMinimumViableRecord()
        {
            // Given

            string natalie = @"Natalie Newplan";

            // When

            double fitness = new SequentialSchemaParsingStrategy().CalculateFitness(natalie);

            // Then

            Assert.AreEqual(0.25, fitness);
        }

        [Test]
        public void CalculateFitnessForUnviableRecord()
        {
            // Given

            string natalie = @"5, 5, 3";

            // When

            double fitness = new SequentialSchemaParsingStrategy().CalculateFitness(natalie);

            // Then

            Assert.AreEqual(0, fitness);
        }

        [Test]
        public void CalculateFitnessForPerfectRecord()
        {
            // Given

            string natalie = @"Natalie Newplan, 7, 4, 6";

            // When

            double fitness = new SequentialSchemaParsingStrategy().CalculateFitness(natalie);
            // Then

            Assert.AreEqual(1, fitness);
        }
    }
}
