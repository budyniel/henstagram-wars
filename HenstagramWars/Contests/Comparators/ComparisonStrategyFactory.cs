﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HenstagramWars.Contests.Comparators
{
    public static class ComparisonStrategyFactory
    {
        public static IComparisonStrategy Create(eStrategy selected)
        {
            switch (selected)
            {
                case eStrategy.RandomlessCharismaLove:
                    return new RandomlessCharismaLoveComparisonStrategy();

                case eStrategy.RandomlessSum:
                    return new RandomlessSumComparisonStrategy();

                default:
                    return new AlwaysLeftComparisonStrategy();
            }
                
        }
    }

    public enum eStrategy
    {
        None = 0,
        RandomlessCharismaLove = 1,
        RandomlessSum = 2
    }
}
