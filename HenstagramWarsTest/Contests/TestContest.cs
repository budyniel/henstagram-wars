using HenstagramWars.Competitors;
using HenstagramWars.Contests;
using HenstagramWars.Contests.Comparators;
using HenstagramWars.Scores;
using NUnit.Framework;
using System.Collections.Generic;

namespace HenstagramWarsTest
{
    public class TestContest
    {

        [Test]
        public void FlatTwoCompetitorsNRDCStrategyGivesHigherToHigher()
        {
            //Given
            List<Competitor> competitors = new List<Competitor>
            {
                new Competitor("Adam", 4,3,4),  //sum -> 11
                new Competitor("Barbara", 1,4,5)   //sum -> 10
            };

            var selectedComparator = new RandomlessSumComparisonStrategy();

            //When
            Score score = new Contest().Run(competitors, selectedComparator);

            //Then
            Assert.IsTrue(score.AllCompetitors[0].Name == "Adam");
            Assert.IsTrue(score.AllCompetitors[0].Score == 3);
            Assert.IsTrue(score.AllCompetitors[1].Name == "Barbara");
            Assert.IsTrue(score.AllCompetitors[1].Score == 1);
            Assert.IsTrue(score.AllCompetitors.Count == 2);
        }

        [Test]
        public void OneCompetitorDoesNotThrowExceptions()
        {
            //Given
            List<Competitor> competitors = new List<Competitor>
            {
                new Competitor("Adam", 4,3,4)
            };

            var selectedComparator = new RandomlessSumComparisonStrategy();

            //When
            Score score = new Contest().Run(competitors, selectedComparator);

            //Then
            Assert.IsTrue(score.AllCompetitors[0].Name == "Adam");
            Assert.IsTrue(score.AllCompetitors[0].Score == 1);
            Assert.IsTrue(score.AllCompetitors.Count == 1);
        }

        [Test]
        public void ZeroCompetitorsDoesNotThrowExceptions()
        {
            //Given
            List<Competitor> competitors = new List<Competitor>
            {
            };

            var selectedComparator = new RandomlessSumComparisonStrategy();

            //When
            Score score = new Contest().Run(competitors, selectedComparator);

            //Then
            Assert.IsTrue(score.AllCompetitors.Count == 0);
        }

        [Test]
        public void FourCompetitorsCharismaStrategyWorksWell_SortingTest()
        {
            //Given
            List<Competitor> competitors = new List<Competitor>
            {
                new Competitor("Barbara", 1,4, charisma:5),
                new Competitor("Adam", 4,3, charisma:4),        // should be last
                new Competitor("Damien", 0,0, charisma:7),      // should be first
                new Competitor("Carol", 999,999, charisma:6)
            };

            var selectedComparator = ComparisonStrategyFactory.Create(eStrategy.RandomlessCharismaLove);

            //When
            Score score = new Contest().Run(competitors, selectedComparator);

            //Then
            Assert.IsTrue(score.AllCompetitors[0].Name == "Damien");        // top score
            Assert.IsTrue(score.AllCompetitors[0].Score == 7);
            Assert.IsTrue(score.AllCompetitors[1].Name == "Carol");
            Assert.IsTrue(score.AllCompetitors[1].Score == 5);
            Assert.IsTrue(score.AllCompetitors[2].Name == "Barbara");
            Assert.IsTrue(score.AllCompetitors[2].Score == 3);
            Assert.IsTrue(score.AllCompetitors[3].Name == "Adam");
            Assert.IsTrue(score.AllCompetitors[3].Score == 1);
            Assert.IsTrue(score.AllCompetitors.Count == 4);
        }

        [Test]
        public void FourCompetitorsCharismaStrategyWorksWell()
        {
            //Given
            List<Competitor> competitors = new List<Competitor>
            {
                new Competitor("Adam", 4,3, charisma:4),
                new Competitor("Barbara", 1,4, charisma:5),
                new Competitor("Carol", 999,999, charisma:6),
                new Competitor("Damien", 0,0, charisma:7)
            };

            var selectedComparator = ComparisonStrategyFactory.Create(eStrategy.RandomlessCharismaLove);

            //When
            Score score = new Contest().Run(competitors, selectedComparator);

            //Then
            Assert.IsTrue(score.AllCompetitors[0].Name == "Damien");
            Assert.IsTrue(score.AllCompetitors[0].Score == 7);
            Assert.IsTrue(score.AllCompetitors[1].Name == "Carol");
            Assert.IsTrue(score.AllCompetitors[1].Score == 5);
            Assert.IsTrue(score.AllCompetitors[2].Name == "Barbara");
            Assert.IsTrue(score.AllCompetitors[2].Score == 3);
            Assert.IsTrue(score.AllCompetitors[3].Name == "Adam");
            Assert.IsTrue(score.AllCompetitors[3].Score == 1);
            Assert.IsTrue(score.AllCompetitors.Count == 4);
        }
    }
}