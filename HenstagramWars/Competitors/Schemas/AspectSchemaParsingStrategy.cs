﻿using CommonTools.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HenstagramWars.Competitors.Schemas
{
    /// <summary>
    /// This parser is quite brittle; it was written to AVOID showing the power of regex but to allow
    /// understanding more or less what is happening.
    /// </summary>
    public class AspectSchemaParsingStrategy : ISchemaParsingStrategy
    {
        public double CalculateFitness(string textToParse)
        {
            // If name can't be extracted, no reason to calculate fitness.
            if (ExtractName(textToParse) == string.Empty)
                return 0;

            (string body, string henhouse, string hen) = ExtractOthers(textToParse);

            double categoryCount = 4; // name, body, henhouse, hen
            double currentScore = 1; // there exists a name

            if (string.IsNullOrEmpty(body) == false) 
                currentScore++;
            if (string.IsNullOrEmpty(henhouse) == false) 
                currentScore++;
            if (string.IsNullOrEmpty(hen) == false) 
                currentScore++;

            return currentScore / categoryCount;
        }

        /// <summary>
        /// Note this method **is** an actual factory method for the Competitor class.
        /// Of course, this can (should) build AspectSchemaPrototype which should further
        /// be adapted into Competitor in CompetitorFactory... but not needed here.
        /// </summary>
        /// <param name="textToParse"></param>
        /// <returns></returns>
        public Competitor Parse(string textToParse)
        {
            string name = ExtractName(textToParse);
            (string body, string henhouse, string hen) = ExtractOthers(textToParse);

            string[] bodyAspects = ToAspects(body);
            string[] henhouseAspects = ToAspects(henhouse);
            string[] henAspects = ToAspects(hen);

            return new Competitor(name,
                henQuality: henAspects.Length,
                henHouse: henhouseAspects.Length,
                charisma: bodyAspects.Length);
        }

        private (string, string, string) ExtractOthers(string textToParse)
        {
            string body = TextParserEngine.SafelyExtractSingleCapturedElement(
                pattern: @"#### Body(.+?)####", text: textToParse);

            string henhouse = TextParserEngine.SafelyExtractSingleCapturedElement(
                pattern: @"#### Henhouse(.+?)####", text: textToParse);

            string hen = TextParserEngine.SafelyExtractSingleCapturedElement(
                pattern: @"#### Hen[^h](.+)", text: textToParse);

            return (body, henhouse, hen);
        }

        private string ExtractName(string textToParse)
        {
            return TextParserEngine.SafelyExtractSingleCapturedElement(
                            pattern: @"### Name(.+?)#", text: textToParse);
        }

        private string[] ToAspects(string mkdnList)
        {
            // Now, USUALLY try-catch like this one here is a horrible antipattern.
            // In real system I use some "get me defaults" and recovery options. 
            // However, this is just a demonstration.
            try
            {
                char[] separators = { '\n', '\r' };
                return mkdnList.Split(separators).Select(r => r.Substring(2)).ToArray();
            }
            catch (Exception)
            {
                return new string[0];
            }

        }
    }
}
