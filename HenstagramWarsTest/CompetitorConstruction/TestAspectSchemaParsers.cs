﻿using HenstagramWars.Competitors;
using HenstagramWars.Competitors.Schemas;
using HenstagramWars.Contests;
using HenstagramWars.Contests.Comparators;
using HenstagramWars.Scores;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HenstagramWarsTest.CompetitorConstruction
{
    public class TestAspectSchemaParsers
    {
        [Test]
        public void ProperlyParseValidRecord()
        {
            // Given

            string arthur = @"### Name

Arthur Chickenson

### Aspects

#### Body

* good looks
* nice voice
* strong like an ox

#### Henhouse

* sturdy construction

#### Hen

* beautiful
* well-fed";

            // When

            Competitor actual = new AspectSchemaParsingStrategy().Parse(arthur);

            // Then

            Assert.AreEqual("Arthur Chickenson", actual.Name);
            Assert.IsTrue(actual.Charisma == 3);
            Assert.IsTrue(actual.HenHouse == 1);
            Assert.IsTrue(actual.HenQuality == 2);
        }


        [Test]
        public void ProperlyParseMinimumValidRecord()
        {
            // Given

            string arthur = @"### Name

Arthur Chickenson

#### Aspects? What are those?
";

            // When

            Competitor actual = new AspectSchemaParsingStrategy().Parse(arthur);

            // Then

            Assert.AreEqual("Arthur Chickenson", actual.Name);
            Assert.IsTrue(actual.Charisma == 0);
            Assert.IsTrue(actual.HenHouse == 0);
            Assert.IsTrue(actual.HenQuality == 0);
        }

        [Test]
        public void CalculateFitnessForMinimumViableRecord()
        {
            // Given

            string arthur = @"### Name

Arthur Chickenson

#### Aspects? What are those?
";

            // When

            double fitness = new AspectSchemaParsingStrategy().CalculateFitness(arthur);

            // Then

            Assert.AreEqual(0.25, fitness);
        }

        [Test]
        public void CalculateFitnessForUnviableRecord()
        {
            // Given

            string arthur = @"### asassd";

            // When

            double fitness = new AspectSchemaParsingStrategy().CalculateFitness(arthur);

            // Then

            Assert.AreEqual(0, fitness);
        }

        [Test]
        public void CalculateFitnessForPerfectRecord()
        {
            // Given

            string arthur = @"### Name

Arthur Chickenson

### Aspects

#### Body

* good looks
* nice voice
* strong like an ox

#### Henhouse

* sturdy construction

#### Hen

* beautiful
* well-fed";

            // When

            double fitness = new AspectSchemaParsingStrategy().CalculateFitness(arthur);

            // Then

            Assert.AreEqual(1, fitness);
        }

    }

}
