﻿using CommonTools.Tools;
using System;
using System.Collections.Generic;
using System.Text;

namespace HenstagramWars.Competitors.Schemas
{
    public class SequentialSchemaParsingStrategy : ISchemaParsingStrategy
    {
        public double CalculateFitness(string textToParse)
        {
            if (ExtractName(textToParse) == string.Empty)
                return 0;

            (string body, string henhouse, string hen) = ExtractOthers(textToParse);

            double categoryCount = 4;
            double currentScore = 1;

            if (string.IsNullOrEmpty(body) == false)
                currentScore++;
            if (string.IsNullOrEmpty(henhouse) == false)
                currentScore++;
            if (string.IsNullOrEmpty(hen) == false)
                currentScore++;

            return currentScore / categoryCount;
        }

        public Competitor Parse(string textToParse)
        {
            string name = ExtractName(textToParse);
            (string hen, string henhouse, string body) = ExtractOthers(textToParse);

            int henValue = int.TryParse(hen, out henValue) ? henValue : 0;
            int henhouseValue = int.TryParse(henhouse, out henhouseValue) ? henhouseValue : 0;
            int bodyValue = int.TryParse(body, out bodyValue) ? bodyValue : 0;

            return new Competitor(name,
                henQuality: henValue,
                henHouse: henhouseValue,
                charisma: bodyValue);
        }

        private (string body, string henhouse, string hen) ExtractOthers(string textToParse)
        {
            string hen = TextParserEngine.SafelyExtractSingleCapturedElement(
                pattern: @"(\d+)", text: textToParse);

            string henhouse = TextParserEngine.SafelyExtractSingleCapturedElement(
                pattern: @"\d+,.(\d+)", text: textToParse);

            string body = TextParserEngine.SafelyExtractSingleCapturedElement(
                pattern: @"\d+,.\d+,.(\d+)", text: textToParse);

            return (hen, henhouse, body);
        }

        private string ExtractName(string textToParse)
        {
            return TextParserEngine.SafelyExtractSingleCapturedElement(
                            pattern: @"^([A-Za-z ]*)(\w*?)", text: textToParse);
        }
    }
}
