﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HenstagramWars.Competitors
{
    public class Competitor
    {
        public Competitor(string name, int henQuality, int henHouse, int charisma)
        {
            Name = name;
            HenQuality = henQuality;
            HenHouse = henHouse;
            Charisma = charisma;
        }

        public string Name { get; }
        public int HenQuality { get; }
        public int HenHouse { get; }
        public int Charisma { get; }
    }
}
