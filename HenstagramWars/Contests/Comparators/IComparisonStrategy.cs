﻿using HenstagramWars.Competitors;
using System;
using System.Collections.Generic;
using System.Text;

namespace HenstagramWars.Contests.Comparators
{
    public interface IComparisonStrategy
    {
        Competitor CalculateWinner(Competitor competitor1, Competitor competitor2);
    }
}
